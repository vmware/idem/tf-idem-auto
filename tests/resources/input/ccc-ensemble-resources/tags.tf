locals {
  tags = {
    "Environment"       = var.profile
    "Owner"             = var.owner
    "product"           = lower(var.product)
    "Region"            = var.region
    "Managed"           = "Terraform"
    "KubernetesCluster" = var.clusterName
  }
}
