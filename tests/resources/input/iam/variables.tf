variable "admin_users" {
  type    = list(string)
  default = [""]
}

variable "owner" {
}

variable "clusterName" {
}

variable "region" {
}

variable "profile" {
}

variable "cogs" {
}

variable "automation" {
  default = true
}

variable "singleAz" {
  default = false
}
