acl: private
environment:
  dev4: development
  prod: production
  scaleperf: scaleperf
  staging: staging
include:
- common-variables
local_tags:
  Environment: test-dev
  KubernetesCluster: idem-test
  Managed: Terraform
  Owner: org1
  Region: eu-west-3
  product: ${lower(var.product)}
product: ''
service: ''
