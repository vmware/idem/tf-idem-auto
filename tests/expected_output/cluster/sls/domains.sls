{% if params["profile"] == "sym-dev4" or params["profile"] == "sym-prod" or params["profile"] == "sym-staging" or params["profile"] == "mango" or params["profile"] == "scaleperf"  %}
# ToDo: The attribute "kwargs" has terraform replace format. Please resolve the attribute value manually
data.aws_route53_zone.internal-potato-beachops-io:
  exec.run:
  - path: aws.route53.hosted_zone.get
  - kwargs:
      name: internal.${replace(var.profile, "/sym-.*/", "potato")}.beachops.io.
      private_zone: true
{% endif %}

{% if params["cross_account_beachops_domain_profile"] != ""  %}
# ToDo: The attribute "kwargs" has terraform replace format. Please resolve the attribute value manually
data.aws_route53_zone.cross-account-internal-potato-beachops-io:
  exec.run:
  - path: aws.route53.hosted_zone.get
  - kwargs:
      provider: ${aws.cross_account_beachops}
      name: "internal.${replace(\n    var.cross_account_beachops_domain_profile,\n\
        \    \"/sym-.*/\",\n    \"potato\",\n  )}.beachops.io."
      private_zone: true
{% endif %}

{% if params["msk_kafka_domain"]  %}
data.aws_route53_zone.msk_kafka_domain:
  exec.run:
  - path: aws.route53.hosted_zone.get
  - kwargs:
      name: kafka.{{ params["region"] }}.amazonaws.com
      private_zone: true
{% endif %}

{% if params["create_vpc"] and params["profile"] == "sym-dev4" or params["profile"] == "sym-prod" or params["profile"] == "sym-staging" or params["profile"] == "mango" or params["profile"] == "scaleperf"  %}
aws_route53_zone_association.internal-potato-beachops-io-0:
  aws.route53.hosted_zone_association.present:
  - resource_id: {{ params.get("aws_route53_zone_association.internal-potato-beachops-io-0")}}
  - name: Z0721725PI001CQXUGYY:vpc-0738f2a523f4735bd:eu-west-3
  - zone_id: Z0721725PI001CQXUGYY
  {% if params["create_vpc"]  %}
  - vpc_id:  ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
  {% else %}
  - vpc_id: ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}
  {% endif %}
  - vpc_region: {{params["region"]}}
  - comment:
{% endif %}

{% if params["msk_kafka_domain"]  %}
aws_route53_zone_association.msk_kafka_domain-0:
  aws.route53.hosted_zone_association.present:
  - resource_id: {{ params.get("aws_route53_zone_association.msk_kafka_domain-0")}}
  - name: Z03616682JNY9P3D3T2IR:vpc-0738f2a523f4735bd:eu-west-3
  - zone_id: Z03616682JNY9P3D3T2IR
  {% if params["create_vpc"]  %}
  - vpc_id:  ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
  {% else %}
  - vpc_id: ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}
  {% endif %}
  - vpc_region: {{params["region"]}}
  - comment:
{% endif %}
