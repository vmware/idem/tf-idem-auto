include:
- sls.cloudwatch
- sls.ec2
- sls.eks
- sls.elasticache
- sls.iam
- sls.rds
- sls.route53
